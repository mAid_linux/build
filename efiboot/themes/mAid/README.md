### Attribution

The initial skeleton of this theme comes from [EvanPurkhiser](https://github.com/EvanPurkhiser/rEFInd-minimal) but there is nothing much left from there ;) .

The mAid icons and this theme itself are part of [mAid](https://maid.binbash.rocks), see LICENSE.md in this folder.

All other icons of this theme are from [Lightness for burg][icons] by [SWOriginal][icon-author].

The background is [Minimalist Wallpaper][wallpaper] by [LeonardoAIanB][wallpaper-author] and adapted in size and with the mAid logo on top.

[icons]: http://sworiginal.deviantart.com/art/Lightness-for-burg-181461810
[icon-author]: http://sworiginal.deviantart.com/

[wallpaper]: http://leonardoalanb.deviantart.com/art/Minimalist-wallpaper-295519786
[wallpaper-author]: http://leonardoalanb.deviantart.com/
