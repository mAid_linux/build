# mAid - [m]anage [A]ndro[id]

## Using mAid

If you are interested in **using/downloading** mAid check the official project documentation (including support options): 

- https://code.binbash.rocks:8443/mAid/build/wiki

## Building mAid

These are the instructions for **_building_** mAid which is meant for **developers** (or the adventurous) only. Users should just download the released ISOs instead (see above topic).

### Requirements

* No other distribution then **Manjaro Linux** is supported. 
* Only supported architecture for mAid is 64 bit.

1. `pacman -S manjaro-tools-iso nbd jq pacman-contribi pesign sbsigntools unzip efitools yay`
1. `yay -S shim-signed`
1. `git clone https://code.binbash.rocks:8443/mAid/build.git ~/build_mAid`


### Setup

Manjaro does not allow a hook to customize a build properly (like the good old archiso tools did) and so you need to hack the main binary.

The following is the smallest possible change but with great powers to extend or overwrite functions within the standard manjaro iso tools.
This way only 1 line needs to be added (at the right place) and everything can be controlled.

edit `/usr/bin/buildiso`:

- find the very last `import` line
- after(!) that last `import` add a new line with this content: `import /home/<YOUR-USER>/build_mAid/mAid.func`

That's it. Now when executing the standard Manjaro build tool all the customization needed will be injected properly.

Drawback: Whenever you update manjaro-tools-iso you have to re-do that change or add manjaro-tools-iso to IgnorePkg in pacman.conf.


#### sudo config

It is recommended not building as user root directly as this is handled by the scripts anyways.

Instead use an unpriv user and give him/her this sudo permissions by adding a file `/etc/sudoers.d/mAid` with the following content:

~~~
############################################################
# allow building
jenkins	ALL=NOPASSWD: /usr/bin/buildiso *
Defaults env_keep += "MAID_VERSION MAID_TYPE MAID_KERNEL MAID_SIZE LANG LC REPO_UPLOAD_ISO_SRV REPO_UPLOAD_ISO_USER REPO_UPLOAD_ISO_PATH"

~~~

If you miss to set the env_keep defaults above the variables will be ignored!


### Usage / Build

1. `cd ~/build_mAid`
1. OPTIONAL: set a remote target to upload the final ISO (all options must be set):
    * `export REPO_UPLOAD_ISO_SRV=<IP/FQDN of a remote ssh server>`
    * `export REPO_UPLOAD_ISO_USER=<ssh user of the remote ssh server>`
    * `export REPO_UPLOAD_ISO_PATH=<target path on the remote ssh server>`
    * tip: if server and/or user are identical to the REPO_UPLOAD_PKG_XX just set the variable here. eg.: `export REPO_UPLOAD_ISO_SRV=$REPO_UPLOAD_PKG_SRV`
1. OPTIONAL: set mAid version and type (if unset version will be set to current date+time and type to "nightly"):
    * `export MAID_VERSION=<version number, e.g: "4.0">`
    * `export MAID_TYPE=<stable|nightly|BETA|RC|...>`
1. OPTIONAL: set mAid kernel (default depends on buildiso default then)
    * `export MAID_KERNEL=<linux510|linux515|...>`
1. OPTIONAL: set mAid build size (default if unspecified: "light")
    * `export MAID_SIZE=full|light`
    * `full` will include all programs so no internet connection needed
    * `light` will include GUI installers which download heavier programs (e.g. easy-installer etc) on demand
1. buildiso kernel: `buildiso -p mAid -r ~/mAid_work -t ~/mAid_work/iso -d xz`
1. specific kernel: `buildiso -k $MAID_KERNEL -p mAid -r ~/mAid_work -t ~/mAid_work/iso -d xz`

Use `buildiso --help` to find all possible options.


### Troubleshooting

If you get an error that `iso-profiles` cannot be found adjust:

`~/.config/manjaro-tools/iso-profiles.conf`

and set the path to `run_dir=/home/<YOUR-USER>/build_mAid/iso-profiles`
