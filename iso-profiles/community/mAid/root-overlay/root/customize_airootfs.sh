#!/bin/bash
#######################################################################################################
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    2017-2024 steadfasterX <steadfasterX | at | gmail - com>
# License:      LGPLv3 - a copy can be found here: /usr/share/licenses/mAid 
#######################################################################################################

set -e -u
echo "selected mAid kernel: $MAID_KERNEL"

echo "nameserver 1.1.1.1" >> /etc/resolv.conf
echo "nameserver 9.9.9.9" >> /etc/resolv.conf

# debug means e.g. SSH server will be added and enabled
# 1 means debug on, 0 off.
DEBUG=0

# when set to 1 it will rebuild packages which might take hours to complete
# e.g. qtwebkit
LONGPKGREBUILD=0

echo "MAID_SIZE: $MAID_SIZE"
mv /etc/locale.${MAID_SIZE} /etc/locale.gen
locale-gen

# hardcode x64, we do not support 32bit anymore
arch=x86_64

# ensure UTF support is avail when building (e.g AUR)
LANG=de_DE.UTF-8

ln -sf /usr/share/zoneinfo/UTC /etc/localtime

usermod -s /bin/bash root
cp -aT /etc/skel/ /root/
chmod 700 /root

#sed -i "s/#Server/Server/g" /etc/pacman.d/mirrorlist
sed -i 's/#\(Storage=\)auto/\1volatile/' /etc/systemd/journald.conf
sed -i 's/#\(HandleSuspendKey=\)suspend/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleHibernateKey=\)hibernate/\1ignore/' /etc/systemd/logind.conf
sed -i 's/#\(HandleLidSwitch=\)suspend/\1ignore/' /etc/systemd/logind.conf

# set buildpkgver
iso_label=mAid_${MAID_TYPE}
iso_version="$MAID_VERSION"
if [ "$iso_label" == "mAid_nightly" -o "${iso_version}" == "nightly" ];then
    buildpkgver="nightly"
else
    buildpkgver="v${iso_version}"
fi

# some vars
TMPSUDOERS=/etc/sudoers.d/build
RSUDOERS=/etc/sudoers.d/mAid
LOGINUSR=android
LOGINPW=linux
RPW=$LOGINPW
if [ "$MAID_SIZE" == "full" ];then
    TINYCLONE="git clone"
else
    TINYCLONE="git clone --depth 1 --no-single-branch"
fi
GIT_CI_BASE_URI="https://github.com/mAid-linux-ci"
GIT_MAID_BRANCH=mAid
ERR=0
TMP_PKGDIR=/var/pkgbuild
PKGVERSION=unknown
REPODIR="/.repo/${MAID_VERSION}"
[ -z "$MAID_SIZE" ] && export MAID_SIZE=light

# set fake release info (get overwritten later)
echo "maidversion=0.0" > /etc/maid-release
echo "maidbuild=0" >> /etc/maid-release
echo "patchlevel=0" >> /etc/maid-release
ln -sf /etc/maid-release /etc/fwul-release

echo "MAID_SIZE: $MAID_SIZE"
echo "iso version: $iso_version"
echo "iso label: $iso_label"

# add live user but ensure this happens when not there already
echo -e "\nuser setup:"
! id $LOGINUSR && useradd -m -p "" -g users -G "adm,audio,floppy,log,network,rfkill,scanner,storage,optical,power,wheel" -s /bin/bash $LOGINUSR
id $LOGINUSR
passwd $LOGINUSR <<EOSETPW
$LOGINPW
$LOGINPW
EOSETPW

##################################
# some funcs

# cleanup all known package caches to start clean
F_CLEANPKG(){
    echo " ... cleaning up"
    PKGVERSION=unknown
    [ -d $TMP_PKGDIR ] && rm -rf $TMP_PKGDIR
    find /var/cache/pacman/pkg/ /var/tmp /home/$LOGINUSR/.cache/yay/ -type f -name *.pkg.tar.* -delete || true
    rm -rf /var/tmp/* || true
    echo " ... cleaning up finished"
}

# make a standard mAid package
# example calls:
#       - F_MAKEPKG easy-installer:AUR
#       - F_MAKEPKG chromium:ALA
# a package base is mandatory and can be either "AUR" (Arch User Repo) or "ALA" (Arch Linux Archive)
F_MAKEPKG(){
    PKGNAME="${1/:*/}"
    PKGBASE="${1/*:/}"

    echo "Starting $FUNCNAME for: $PKGNAME"

    # ensure we start clean
    F_CLEANPKG
    mkdir $TMP_PKGDIR

    # install requested package depending on the specified base
    case $PKGBASE in
        AUR|aur) pacman -Q $PKGNAME || su -c - $LOGINUSR "yay -S --builddir=/var/tmp --noconfirm $PKGNAME" ;;
        ALA|ala) RMN=1; pacman -Q $PKGNAME || RMN=0; if [ $RMN == "0" ];then pacman -S --noconfirm $PKGNAME ; fi ;;
        *) echo "ERROR: You have to specify a package base (AUR|ALA)!" && return 4 ;;
    esac

    # move the main package
    find /var/cache/pacman/pkg/ /var/tmp /home/$LOGINUSR/.cache/yay/ -name "${PKGNAME}*.pkg.tar.zst" -exec mv -v {} $REPODIR/ \;

    # also move the required dependencies if they were installed during the process
    for req in $(LC_ALL=C pacman -Qi $PKGNAME | grep Depends |cut -d ":" -f2);do
        find /var/cache/pacman/pkg/ /var/tmp /home/$LOGINUSR/.cache/yay/ -name "${req}*.pkg.tar.zst" -exec mv -v {} $REPODIR/ \;
    done

    # check file sizes so we don't add zero byte files
    for t in $(find $REPODIR -type f); do  F_CHKFSIZE "$t" ; done || return 3

    # identify its version
    PKGVERSION=$(pacman -Q $PKGNAME | cut -d " " -f2)

    # remove everything when mAid light
    if [ "$MAID_SIZE" == "light" ];then
        echo -e "\nremove pkg and its deps (HINT THIS COULD REMOVE MORE THEN GOOD):\n"
        pacman -Rnscu --noconfirm $PKGNAME
    fi
}

# standard git clone including error handling and tiny clone
F_GITCLONE(){
    RETRIES=0
    while [ $RETRIES -lt 10 ];do
        $TINYCLONE $@ && break
        ERR=$?
        echo "$FUNCNAME: problem occured fetching $1.."
        RETRIES=$((RETRIES +1))
        sleep 15
    done
    return $ERR
}

# check the size of a given file including error handling
F_CHKFSIZE(){
    F=$1
    [ ! -f "$F" ] && echo "ERROR: file $F does not exist!" && return 3
    FSIZE=$(stat --printf=%s "$F")
    [ "$FSIZE" -eq 0 ] && echo "ERROR: file $F has a size of 0!" && return 5
    echo "... |- size OK: $F"
}

###################################################################
# prepare user home !!! No changes above!
#######################
cp -avT /etc/fwul/ /home/$LOGINUSR/
[ ! -d /home/$LOGINUSR/Desktop ] && mkdir /home/$LOGINUSR/Desktop
chmod 700 /home/$LOGINUSR
chown -R $LOGINUSR /home/$LOGINUSR 

# ensure a re-run will not break
[ -d "/home/$LOGINUSR/.cache/yay" ] && rm -rf /home/$LOGINUSR/.cache/yay
pacman -Q gcc || pacman -S --needed --noconfirm gcc make

# prepare hosts
[ ! -f /etc/hosts.orig ] && cp /etc/hosts /etc/hosts.orig
echo "10.0.228.17     leech.binbash.rocks" >> /etc/hosts
echo "10.0.228.12     code.binbash.rocks" >> /etc/hosts

# add user to required groups
usermod -a -G vboxsf $LOGINUSR 

# session hacks
sed -i "s#REPLACEVBOXHOME#/home/$LOGINUSR/Desktop#g" /etc/systemd/scripts/fwul-session.sh
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /etc/profile.d/maid-language.sh
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /home/$LOGINUSR/.xprofile
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /home/$LOGINUSR/.fwul/*.sh
sed -i "s#LOGINUSR#$LOGINUSR#g" /home/$LOGINUSR/.fwul/maid-session.sh
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /usr/share/polkit-1/actions/*.policy
sed -i "s#REPLACEHOME#/home/$LOGINUSR#g" /etc/dconf/db/database.d/*

# Thunar settings
[ ! -d /home/$LOGINUSR/.config/Thunar ] && mkdir -p /home/$LOGINUSR/.config/Thunar
# fix exo-open by using gnome-terminal:
cp /home/$LOGINUSR/.fwul/uca.xml /home/$LOGINUSR/.config/Thunar/

# temp perms for archiso
[ -f $RSUDOERS ]&& rm -vf $RSUDOERS      # ensures an update build will not fail
cat > $TMPSUDOERS <<EOSUDOERS
ALL     ALL=(ALL) NOPASSWD: ALL
EOSUDOERS

# add the pacman ignore list
# https://code.binbash.it:8443/FWUL/build_fwul/issues/63
sed -i '/\[options\]/a Include=/etc/pacman.ignore' /etc/pacman.conf

cat > /etc/pacman.ignore<<EOPIGN
# PARTIAL UPGRADES ARE NOT SUPPORTED! USE THIS WITH CARE!
# Check this first:
# https://wiki.archlinux.org/index.php/System_maintenance#Partial_upgrades_are_unsupported

# The option IgnorePkg EXTENDS the pacman ignore list.
# Format: ONE package for every ignore line! Do *not* use space delimitated syntax here!

# uefi and syslinux
IgnorePkg = efibootmgr
IgnorePkg = efitools
IgnorePkg = efivar
IgnorePkg = refind-efi
IgnorePkg = syslinux

# kernel, kernel headers and ramdisk tools
IgnorePkg = mkinitcpio-busybox
IgnorePkg = mkinitcpio
IgnorePkg = linux*

# virtualbox module
IgnorePkg = virtualbox-guest-utils

# extreme CPU intensive and usually not needed to upgrade
IgnorePkg = qtwebkit
EOPIGN

# just install locales and manpages for the supported languages:
echo -e "\nSetup needed locale & MAN pages only:"
sed -i '/\[options\]/a Include=/etc/pacman.locale' /etc/pacman.conf
KEEP1=$(cut -d '_' -f 1 /etc/locale.gen | uniq | grep -E -v "#\s+|#$" | sed -E 's#(^)(\w*)#\!usr/share/locale/\1\2*#g' | tr '\n' ' ')
KEEP2=$(cut -d '_' -f 1 /etc/locale.gen | uniq | grep -E -v "#\s+|#$" | sed -E 's#(^)(\w*)#\!usr/share/i18n/locales/\1\2*#g' | tr '\n' ' ')
KEEP3=$(cut -d '_' -f 1 /etc/locale.gen | uniq | grep -E -v "#\s+|#$" | sed -E 's#(^)(\w*)#\!usr/share/X11/locale/\1\2*#g' | tr '\n' ' ')
KEEP4=$(cut -d '_' -f 1 /etc/locale.gen | uniq | grep -E -v "#\s+|#$" | sed -E 's#(^)(\w*)#\!usr/share/vim/vim*/lang/\1\2*#g' | tr '\n' ' ')
cat > /etc/pacman.locale << _EOPU
NoExtract = usr/share/locale/* !usr/share/locale/locale.alias !usr/share/*locale*/locale.* $KEEP1
NoExtract = usr/share/i18n/locales/* !usr/share/i18n/locales/i18n* !usr/share/i18n/locales/iso* !usr/share/i18n/locales/translit* !usr/share/i18n/charmaps/* $KEEP2
NoExtract = usr/share/X11/locale/* $KEEP3
NoExtract = usr/share/vim/vim*/lang/* $KEEP4
NoExtract = usr/share/man/* !usr/share/man/man*
NoExtract = usr/share/gtk-doc/html/*
_EOPU
cat /etc/pacman.locale

if [ "$MAID_SIZE" == "light" ];then
    # reduce linux-firmware minimal
    sed -i '/\[options\]/a Include=/etc/pacman.reduce' /etc/pacman.conf
    cat > /etc/pacman.reduce << _EOPMIN
NoExtract = !usr/lib/firmware/* usr/lib/firmware/netronome/* usr/lib/firmware/mellanox/* usr/lib/firmware/liquidio/*
_EOPMIN
fi

# init pacman + multilib
# O M G ! This is so crappy bullshit! when build.sh see's 
# the returncode 1 it just stops?!! so I use that funny workaround..
RET=$(grep -E -q '^\[multilib' /etc/pacman.conf||echo missing)
if [ "$RET" == "missing" ];then
    echo "adding multilib to conf"
    cat >>/etc/pacman.conf<<EOPACMAN
[multilib]
SigLevel = PackageRequired
Include = /etc/pacman.d/mirrorlist
EOPACMAN
else
    echo skipping multilib because it is configured already
fi

# disable free disk check
sed -i 's/^CheckSpace/#CheckSpace/' /etc/pacman.conf

# add mAid repo (must happen before installing packages)
MAID_REPO_URL="https://leech.binbash.rocks:8008/mAid${REPODIR}"
sed -i "/\[core\]/i # mAid repo: must stay above the others to avoid conflicts\n[mAid-${MAID_VERSION}]\nServer = $MAID_REPO_URL\nSigLevel = Required DatabaseOptional TrustedOnly\n\n#####################################\n\n\n" /etc/pacman.conf

# signing keys for mAid packages & repo
pacman-key --recv-keys --keyserver hkp://keys.gnupg.net 89DBB3A516A1A9FD D97F436B1A616F5D 75F507E560664923
# set trust (lsign does not work properly)
echo -e "5\ny\n" | gpg --no-tty --homedir /etc/pacman.d/gnupg/ --command-fd 0 --expert --edit-key archpkgs@binbash.rocks trust;
echo -e "5\ny\n" | gpg --no-tty --homedir /etc/pacman.d/gnupg/ --command-fd 0 --expert --edit-key steadfasterX@gmail.com trust;
echo -e "5\ny\n" | gpg --no-tty --homedir /etc/pacman.d/gnupg/ --command-fd 0 --expert --edit-key maid@binbash.rocks trust;

# initialize the needed keyrings (again)
pacman -Sy --noconfirm gnupg archlinux-keyring manjaro-keyring
#haveged -w 1024
pacman-key --init
pacman-key --populate archlinux manjaro

# fix pacman gpg perms
chmod 755 /etc/pacman.d/gnupg 

# make a repo mirrorlist
pacman-mirrors -t 0 --geoip

# update first.
# .. but sometimes the AUR has timeout issues:
RETRIES=0
while [ $RETRIES -lt 10 ];do
    su -c - $LOGINUSR "yay -Syyu --noconfirm --needed" && break
    ERR=$?
    echo "update ended with $ERR"
    RETRIES=$((RETRIES +1))
    sleep 30
done

#
# preparing system finished - no regular install stuff above!
#
####################################################################

######################################
# install from TESTING branch
# see https://wiki.manjaro.org/index.php?title=Switching_Branches
# if you change packages here, do not forget doing that in calamares/modules/custom_inchroot.conf, too!

DEPKGS=""

# switch to testing
#pacman-mirrors --api --set-branch testing && sudo pacman-mirrors --fasttrack 5 && sudo pacman -Syy

# install the testing packages
#pacman -S --noconfirm $DEPKGS

# switch back to stable
#pacman-mirrors --api --set-branch stable && sudo pacman-mirrors --fasttrack 5 && sudo pacman -Syy

# end: install from TESTING branch
######################################

############################################
# install all needed AUR packages

# dbus special handling
# fixes: "dbus-x11 and dbus are in conflict. Remove dbus?"
#DR=1
#pacman -Q |grep -E 'dbus ' || DR=0
#if [ $DR -eq 1 ];then pacman --noconfirm -Rns -dd dbus; else true; fi

#su -c - $LOGINUSR "yay -S --noconfirm $(cat /Packages-AUR | grep -v '#' | tr '\n' ' ')"
rm /Packages-AUR
############################################

echo "CURRENT MOUNTS"
df -h

# installing mAid + its deps
su -c - $LOGINUSR "yay -S --noconfirm bootimgtool-git bibata-extra-cursor-theme testdisk-wip"
su -c - $LOGINUSR "yay -S --noconfirm --needed maid-desktop-${MAID_SIZE}"

# grab package versions
SONYVERS=$(pacman -Q xperia-flashtool | cut -d " " -f2)
JDVERS=$(pacman -Q jodin3-bin | cut -d " " -f2)
TVVERS=$(pacman -Q teamviewer | cut -d " " -f2)
SPFTVERS=$(pacman -Q spflashtool-bin | cut -d " " -f2)
CHRVERS=$(pacman -Q chromium | cut -d " " -f2)
FFVERS=$(pacman -Q firefox | cut -d " " -f2)
EIVERS=$(pacman -Q easy-installer | cut -d " " -f2)
JREVERS=$(pacman -Q jre8 | cut -d " " -f2)

if [ "$MAID_SIZE" == "full" ];then
    [ -z "$SONYVERS" ]  && echo "ERROR: missing sony version!" && exit 3
    [ -z "$JDVERS" ]    && echo "ERROR: missing jodin3 version!" && exit 3
    [ -z "$TVVERS" ]    && echo "ERROR: missing teamviewer version!" && exit 3
    [ -z "$SPFTVERS" ]  && echo "ERROR: missing spflashtool version!" && exit 3
    [ -z "$CHRVERS" ]   && echo "ERROR: missing chromium version!" && exit 3
    [ -z "$FFVERS" ]    && echo "ERROR: missing firefox version!" && exit 3
    [ -z "$EIVERS" ]    && echo "ERROR: missing easyinstaller version!" && exit 3
    [ -z "$JREVERS" ]   && echo "ERROR: missing jre8 version!" && exit 3
fi

# add main browser to desktop
cp /usr/share/applications/org.midori_browser.Midori.desktop /home/$LOGINUSR/Desktop/midori.desktop

# add terminal to desktop
cp /usr/share/applications/org.gnome.Terminal.desktop /home/$LOGINUSR/Desktop/terminal.desktop

# GUI based package manager
cp /usr/share/applications/org.manjaro.pamac.manager.desktop /home/$LOGINUSR/Desktop/pamac-manager.desktop
sed -i 's/Icon=.*/Icon=gnome-software/g' /home/$LOGINUSR/Desktop/pamac-manager.desktop

# disable tray to avoid bothering users for updating
[ -f /etc/xdg/autostart/pamac-tray.desktop ] && rm /etc/xdg/autostart/pamac-tray.desktop

# prepare Samsung tool dir
[ ! -d /home/$LOGINUSR/Desktop/Samsung ] && mkdir /home/$LOGINUSR/Desktop/Samsung

# install udev-rules
[ ! -d /home/$LOGINUSR/.android ] && mkdir /home/$LOGINUSR/.android
[ ! -f /home/$LOGINUSR/.android/adb_usb.ini ] && wget https://github.com/M0Rf30/android-udev-rules/raw/704f83d8528a56c969dab8a9b63bf83279d4c71b/adb_usb.ini -O /home/$LOGINUSR/.android/adb_usb.ini

# always update the udev rules to be top current
wget https://raw.githubusercontent.com/M0Rf30/android-udev-rules/master/51-android.rules -O /etc/udev/rules.d/51-android.rules

# add adb group & perms
groupadd -f adbusers
usermod -a -G adbusers $LOGINUSR

# install & prepare tmate
echo -e "\ntmate:"
# install the ssh keygenerator
[ ! -d /home/$LOGINUSR/.config/autostart ] && mkdir -p /home/$LOGINUSR/.config/autostart && chown -R $LOGINUSR /home/$LOGINUSR/.config/autostart
echo -e '#!/bin/bash\n[ !'" -f /home/$LOGINUSR/.ssh/id_rsa ] && ssh-keygen -a 100 -t ed25519 -f /home/$LOGINUSR/.ssh/id_ed25519 -P ''" > /home/$LOGINUSR/.fwul/sshkeygen.sh
cat > /home/$LOGINUSR/.config/autostart/sshkeygen.desktop<<EOSSHKG
[Desktop Entry]
Version=1.0
Type=Application
Comment=sshkeygen
Terminal=false
Name=sshkeygen
Exec=/home/$LOGINUSR/.fwul/sshkeygen.sh
EOSSHKG
chmod +x /home/$LOGINUSR/.fwul/sshkeygen.sh

# ensure the screenlock background gets set properly
cat > /home/$LOGINUSR/.config/autostart/z_setlockbg.desktop <<EOLBG
[Desktop Entry]
Version=1.0
Type=Application
Comment=autostarted on login
Terminal=false
Name=set background for screenlock
Exec=/home/$LOGINUSR/.xprofile
EOLBG
chmod +x /home/$LOGINUSR/.xprofile

# install & add Heimdall
echo -e "\nheimdall:"
cp /usr/share/applications/heimdall.desktop /home/$LOGINUSR/Desktop/Samsung/
# fix missing heimdall icon
sed -i "s#Icon=.*#Icon=/home/$LOGINUSR/.fwul/heimdall.png#g" /home/$LOGINUSR/Desktop/Samsung/heimdall.desktop

# install testdisk/photorec incl. GUI support
# (https://code.binbash.it:8443/FWUL/build_fwul/issues/66)
echo -e "\nphotorec:"
cp -v /usr/share/applications/qphotorec.desktop /home/$LOGINUSR/Desktop/
chmod +x /home/$LOGINUSR/Desktop/qphotorec.desktop 
sed -i "s#Icon=.*#Icon=/home/$LOGINUSR/.fwul/qphotorec.png#g" /home/$LOGINUSR/Desktop/qphotorec.desktop
sed -i 's#^Exec=.*#Exec=sudo /usr/bin/qphotorec %F#g' /home/$LOGINUSR/Desktop/qphotorec.desktop
chown $LOGINUSR /home/$LOGINUSR/Desktop/qphotorec.desktop

# add the mAid support link
echo -e "\nsupport link:"
cat > /home/$LOGINUSR/Desktop/support.desktop<<EOSUP
[Desktop Entry]
Version=1.0
Type=Application
Terminal=false
Name=mAid Support
Comment=Open the support chat on matrix.org
Exec=xdg-open https://app.element.io/#/room/#mAid_Linux:binbash.rocks
Icon=/home/android/programs/welcome/icons/welcome.png
EOSUP
chmod +x /home/$LOGINUSR/Desktop/support.desktop
chown $LOGINUSR /home/$LOGINUSR/Desktop/support.desktop

# install welcome screen
echo -e "\nwelcome-screen:"
if [ ! -d /home/$LOGINUSR/programs/welcome ];then
    F_GITCLONE "https://code.binbash.rocks:8443/mAid/welcome.git" "/home/$LOGINUSR/programs/welcome"
    # install the regular welcome screen
    if [ ! -f /home/$LOGINUSR/.config/autostart/welcome.desktop ];then
        [ ! -d /home/$LOGINUSR/.config/autostart ] && mkdir -p /home/$LOGINUSR/.config/autostart && chown -R $LOGINUSR /home/$LOGINUSR/.config/autostart
        cat > /home/$LOGINUSR/.config/autostart/welcome.desktop<<EOWAS
[Desktop Entry]
Version=1.0
Type=Application
Comment=mAid Welcome Screen
Terminal=false
Name=Welcome
Exec=/home/$LOGINUSR/programs/welcome/welcome.sh
Icon=/home/$LOGINUSR/programs/welcome/icons/welcome.png
EOWAS
    fi
    # install the force welcome screen (when user manually want to start it)
    if [ ! -f /home/$LOGINUSR/Desktop/welcome.desktop ];then
        cat > /home/$LOGINUSR/Desktop/welcome.desktop <<EOWASF
[Desktop Entry]
Version=1.0
Type=Application
Comment=mAid Welcome Screen
Terminal=false
Name=Welcome
Exec=/home/$LOGINUSR/programs/welcome/welcome-force.sh
Icon=/home/$LOGINUSR/programs/welcome/icons/welcome.png
EOWASF
    fi
    chmod +x /home/$LOGINUSR/.config/autostart/welcome.desktop /home/$LOGINUSR/Desktop/welcome.desktop
fi

# install JOdin3
echo "JOdin:"
if [ $arch == "x86_64" ];then
    if [ ! -d /home/$LOGINUSR/programs/JOdin ];then
        mkdir /home/$LOGINUSR/programs/JOdin
        cat >/home/$LOGINUSR/programs/JOdin/starter.sh <<EOEXECOD
#!/bin/bash
pacman -Q jre8 || sudo \$HOME/.fwul/install_package.sh pacman jre8
sudo archlinux-java set java-11-openjdk
JAVA_HOME=/usr/lib/jvm/java-8-jre /home/$LOGINUSR/programs/JOdin/JOdin3CASUAL
EOEXECOD
        chmod +x /home/$LOGINUSR/programs/JOdin/starter.sh
	echo before downloadin:
        wget --no-check-certificate "https://leech.binbash.rocks:8008/misc/JOdin3CASUAL-r1142-dist.tar.gz" -O JOdin.tgz
	ls -la JOdin.tgz
       	file JOdin.tgz
        tar -xzf JOdin.tgz -C /home/$LOGINUSR/programs/JOdin/ && rm -rf /home/$LOGINUSR/programs/JOdin/runtime JOdin.tgz
        cat >/home/$LOGINUSR/Desktop/Samsung/JOdin.desktop <<EOODIN
[Desktop Entry]
Version=1.0
Type=Application
Comment=Odin for Linux
Terminal=false
Name=JOdin3
Exec=/home/$LOGINUSR/programs/JOdin/starter.sh
Icon=/home/$LOGINUSR/.fwul/odin-logo.jpg
EOODIN
        chmod +x /home/$LOGINUSR/Desktop/Samsung/JOdin.desktop
    fi
else
    echo "SKIPPING JODIN INSTALL: Arch $arch detected!"
fi

# JOdin3 (2021) installer
cat >/home/$LOGINUSR/Desktop/Samsung/install-jodin3-2021.desktop << _EOJOD
[Desktop Entry]
Version=1.0
Type=Application
Comment=Install Jodin3 (2021 version)
Terminal=false
Name=JOdin3 installer (newer devices)
Exec=/home/$LOGINUSR/.fwul/install_jodin3.sh
Icon=anaconda
_EOJOD
chmod +x /home/$LOGINUSR/Desktop/Samsung/install-jodin3-2021.desktop

# chromium installer
cat >/home/$LOGINUSR/Desktop/install-chromium.desktop <<EOsflashinst
[Desktop Entry]
Version=1.0
Type=Application
Comment=Chromium Browser Installer
Terminal=false
Name=Chromium installer
Exec=/home/$LOGINUSR/.fwul/install_chromium.sh
Icon=anaconda
EOsflashinst
chmod +x /home/$LOGINUSR/Desktop/install-chromium.desktop
# workaround as otherwise chromium will start
# only when logout - login again
su -c - $LOGINUSR "echo -n $LOGINPW | gnome-keyring-daemon --unlock"

# firefox installer
cat >/home/$LOGINUSR/Desktop/install-ff.desktop <<EOff
[Desktop Entry]
Version=1.0
Type=Application
Comment=Firefox Browser Installer
Terminal=false
Name=Firefox installer
Exec=/home/$LOGINUSR/.fwul/install_ff.sh
Icon=anaconda
EOff
chmod +x /home/$LOGINUSR/Desktop/install-ff.desktop

cat >/home/$LOGINUSR/.fwul/sonyflash.desktop <<EOsflash
[Desktop Entry]
Version=1.0
Type=Application
Comment=Sony FlashTool
Terminal=false
Name=Sony Flashtool
Exec=xperia-flashtool
Icon=/home/$LOGINUSR/.fwul/flashtool-icon.png
EOsflash

# teamviewer installer
echo -e "\nteamviewer:"
cat >/home/$LOGINUSR/Desktop/install-TV.desktop <<EOODIN
[Desktop Entry]
Version=1.0
Type=Application
Comment=Teamviewer installer
Terminal=false
Name=TeamViewer Installer
Exec=/home/$LOGINUSR/.fwul/install_tv.sh
Icon=anaconda
EOODIN
chmod +x /home/$LOGINUSR/Desktop/install-TV.desktop

# SP Flash Tools installer
echo -e "\nSP Flash Tools:"
cat >/home/$LOGINUSR/Desktop/install-spflash.desktop <<EOSPF
[Desktop Entry]
Version=1.0
Type=Application
Comment=SP FlashTools installer
Terminal=false
Name=SP FlashTools Installer
Exec=/home/$LOGINUSR/.fwul/install_spflash.sh
Icon=anaconda
EOSPF
chmod +x /home/$LOGINUSR/Desktop/install-spflash.desktop

# Sony Flash Tools installer
echo -e "\nSony Flash Tools:"
cat >/home/$LOGINUSR/Desktop/install-sonyflash.desktop <<EOSFT
[Desktop Entry]
Version=1.0
Type=Application
Comment=Sony FlashTools installer
Terminal=false
Name=Sony FlashTools Installer
Exec=/home/$LOGINUSR/.fwul/install_sonyflash.sh
Icon=anaconda
EOSFT
chmod +x /home/$LOGINUSR/Desktop/install-sonyflash.desktop

# prepare LG tools
[ ! -d /home/$LOGINUSR/Desktop/LG ] && mkdir /home/$LOGINUSR/Desktop/LG/

echo -e "\nSALT:"
[ ! -d /home/$LOGINUSR/programs/SALT ] && F_GITCLONE https://github.com/steadfasterX/salt.git /home/$LOGINUSR/programs/SALT
[ ! -d /home/$LOGINUSR/programs/lglafng ] && F_GITCLONE https://github.com/steadfasterX/lglaf.git /home/$LOGINUSR/programs/lglafng
[ ! -d /home/$LOGINUSR/programs/kdztools ] && F_GITCLONE https://github.com/steadfasterX/kdztools.git /home/$LOGINUSR/programs/kdztools
[ ! -d /home/$LOGINUSR/programs/sdat2img ] && F_GITCLONE https://github.com/xpirt/sdat2img.git /home/$LOGINUSR/programs/sdat2img

# checkout the right branch depending on nightly or not
# this uses salt.vars to stay flexible with branch names (just for the case..)
grep 'BR_._.*' /home/$LOGINUSR/programs/SALT/salt.vars > /tmp/saltvars
source /tmp/saltvars
rm /tmp/saltvars
SALTPATH=/home/$LOGINUSR/programs/SALT
LAFPATH=/home/$LOGINUSR/programs/lglafng
KDZTOOLS=/home/$LOGINUSR/programs/kdztools
case $MAID_VERSION in
    nightly) echo "SALT: switching to nightly"; BRSALT="$BR_T_SALT"; BRKDZ="$BR_T_KDZ"; BRLAF="$BR_T_LAF" ;;
    *)       echo "SALT: switching to stable ($MAID_VERSION)"; BRSALT="$BR_S_SALT"; BRKDZ="$BR_S_KDZ"; BRLAF="$BR_S_LAF" ;;
esac
for u in ${SALTPATH}:$BRSALT ${LAFPATH}:$BRLAF ${KDZTOOLS}:$BRKDZ; do
    echo "updating ${u/:*} on branch ${u/*:} ..."
    cd ${u/:*}
    echo "$(git checkout ${u/*:} 2>&1 || git config --global --add safe.directory ${u/:*} 2>&1)"
    git pull 2>&1
done
cd /

if [ ! -f /home/$LOGINUSR/Desktop/LG/SALT.desktop ];then
    cat > /home/$LOGINUSR/Desktop/LG/SALT.desktop <<EOFDSK
[Desktop Entry]
Version=1.0
Type=Application
Terminal=false
Name=SALT
Icon=/home/$LOGINUSR/programs/SALT/icons/salt_icon.png
Comment=SALT - [S]teadfasterX [A]ll-in-one [L]G [T]ool
Exec=pkexec /home/$LOGINUSR/programs/SALT/salt
EOFDSK
fi
chmod +x /home/$LOGINUSR/Desktop/LG/*.desktop

# ensure updating SALT will work as root user
git config --global --add safe.directory /home/$LOGINUSR/programs/kdztools
git config --global --add safe.directory /home/$LOGINUSR/programs/lglafng
git config --global --add safe.directory /home/$LOGINUSR/programs/SALT

# pure LG LAF
echo -e "\npure lglaf:"
[ ! -d /home/$LOGINUSR/programs/lglaf ] && F_GITCLONE https://github.com/Lekensteyn/lglaf.git /home/$LOGINUSR/programs/lglaf

# LG LAF shortcut with auth
echo -e "\nLG LAF shortcut with auth:"
cat >/home/$LOGINUSR/Desktop/LG/open-lglafshell.desktop <<EOSFT
[Desktop Entry]
Version=1.0
Type=Application
Comment=LG LAF
Terminal=false
Name=LG LAF (PeterWu)
Exec=gnome-terminal -- bash -c "cd /home/$LOGINUSR/programs/lglaf; exec bash"
Icon=terminal
EOSFT
chmod +x /home/$LOGINUSR/Desktop/LG/open-lglafshell.desktop

# LGLAF (steadfasterX) 
echo -e "\nLG LAF NG shortcut:"
cat >/home/$LOGINUSR/Desktop/LG/open-lglafng.desktop <<EOLAFNG
[Desktop Entry]
Version=1.0
Type=Application
Comment=LG LAF with steadfasterX patches
Terminal=false
Name=LG LAF (steadfasterX)
Exec=gnome-terminal -- bash -c "cd /home/$LOGINUSR/programs/lglafng; exec bash"
Icon=terminal
EOLAFNG

# LGLAF (runningnak3d)
[ ! -d /home/$LOGINUSR/programs/lglafsploit ] && F_GITCLONE https://gitlab.com/runningnak3d/lglaf.git /home/$LOGINUSR/programs/lglafsploit
echo -e "\nLG LAF lafsploit shortcut:"
cat >/home/$LOGINUSR/Desktop/LG/open-lglafsploit.desktop <<EOLAFNG
[Desktop Entry]
Version=1.0
Type=Application
Comment=LG LAF with runningnak3d patches
Terminal=false
Name=LG LAF (runningnak3d)
Exec=gnome-terminal -- bash -c "cd /home/$LOGINUSR/programs/lglafsploit; exec bash"
Icon=terminal
EOLAFNG

# tmate
echo -e "\ntmate shortcut:"
cat >/home/$LOGINUSR/Desktop/tmate.desktop <<EOTMATE
[Desktop Entry]
Name=tmate - Simple Remote Control
GenericName=tmate
Comment=Terminal sharing with tmate
Exec=gnome-terminal --maximize -- bash -c "/home/$LOGINUSR/.fwul/tmate.sh"
Icon=/home/$LOGINUSR/.fwul/tmate-logo.png
Terminal=false
Type=Application
EOTMATE
chmod +x /home/$LOGINUSR/.fwul/tmate.sh

# easy-installer
cat >/home/$LOGINUSR/Desktop/install-ei.desktop <<EOff
[Desktop Entry]
Version=1.0
Type=Application
Comment=Easy Installer (flash Android /e/ on supported devices)
Terminal=false
Name=Easy Installer
Exec=/home/$LOGINUSR/.fwul/install_ei.sh
Icon=anaconda
EOff
chmod +x /home/$LOGINUSR/Desktop/install-ei.desktop

# install display manager
echo -e "\nDM:"
systemctl enable lightdm

# configure login/display manager
cp -v /home/$LOGINUSR/.fwul/lightdm-gtk-greeter.conf /etc/lightdm/
# copy background + icon for greeter
cp -v /home/$LOGINUSR/.fwul/fwul_login.png /usr/share/pixmaps/greeter_background.png
mv /home/$LOGINUSR/.fwul/greeter_icon.png /usr/share/pixmaps/

# Special things needed for easier DEBUGGING
if [ "$DEBUG" -eq 1 ];then
    pacman -Q openssh || pacman -S --noconfirm openssh
    systemctl enable sshd
    pacman -Q spice-vdagent || pacman -S --noconfirm spice-vdagent
    systemctl enable spice-vdagentd
fi

# add simple ADB 
# (https://forum.xda-developers.com/android/software/revive-simple-adb-tool-t3417155, https://github.com/mhashim6/Simple-ADB)
cat >/home/$LOGINUSR/Desktop/ADB.desktop <<EOSADB
[Desktop Entry]
Version=1.0
Type=Application
Comment=adb and fastboot GUI
Terminal=false
Name=Simple-ADB
Exec=/home/$LOGINUSR/programs/sadb/starter.sh
Icon=/home/$LOGINUSR/programs/sadb/sadb.jpg
EOSADB

cat >/home/$LOGINUSR/programs/sadb/starter.sh <<EOEXECADB
#!/bin/bash
pacman -Q jre8 || sudo \$HOME/.fwul/install_package.sh pacman jre8
java -jar /home/$LOGINUSR/programs/sadb/S-ADB.jar
EOEXECADB
chmod +x /home/$LOGINUSR/programs/sadb/starter.sh

# install Xiaomi MiFlash
# https://code.binbash.it:8443/FWUL/build_fwul/issues/62
if [ ! -d /home/$LOGINUSR/programs/MiFlash ];then
    F_GITCLONE https://github.com/limitedeternity/MiFlash-Linux.git /home/$LOGINUSR/programs/MiFlash
    # ensure the stupid setup do not run 
    > /home/$LOGINUSR/.xiaomi_tool
    # fix help
    ln -s /home/$LOGINUSR/programs/MiFlash/README.md /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash/.bin/xiaomi_tools/README.txt
    sed -i 's/gedit/leafpad/g' /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash/.bin/xiaomi_tools/xiaomi_tools.cfg
    # catch the icon
    wget -O /home/$LOGINUSR/programs/MiFlash/Xiaomi.png https://www.xiaomiflash.com/img/Xiaomi.png
    # make it usable ( - do not use go.sh - )
    cat >/home/$LOGINUSR/programs/MiFlash/starter.sh <<EOEXECXIA
#!/bin/bash
cd /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash
gnome-terminal --maximize -- bash -c "cd /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash; /home/$LOGINUSR/programs/MiFlash/Xiaomi_MiFlash/.bin/xiaomi_tools/xiaomi_tools.cfg"
EOEXECXIA
    chmod +x /home/$LOGINUSR/programs/MiFlash/starter.sh

    [ ! -d /home/$LOGINUSR/Desktop/Xiaomi/ ] && mkdir /home/$LOGINUSR/Desktop/Xiaomi  && chown $LOGINUSR /home/$LOGINUSR/Desktop/Xiaomi
    cat >/home/$LOGINUSR/Desktop/Xiaomi/miflash.desktop <<EODXIA
[Desktop Entry]
Version=1.0
Type=Application
Comment=
Terminal=false
Name=Xiaomi MiFlash
Exec=/home/$LOGINUSR/programs/MiFlash/starter.sh
Icon=/home/$LOGINUSR/programs/MiFlash/Xiaomi.png
Path=/home/$LOGINUSR/programs/MiFlash/
EODXIA
    # working dir shortcut
    echo -e "\nMiFlash working dir shortcut:"
    ln -s /home/$LOGINUSR/programs/MiFlash/ /home/$LOGINUSR/Desktop/Xiaomi/MiFlash_WorkDir
fi

# install payload extractor
[ ! -d /home/$LOGINUSR/programs/ROME ] && F_GITCLONE https://code.binbash.rocks:8443/mAid/android_rome.git /home/$LOGINUSR/programs/ROME
echo -e "\nROME shortcut:"
cat >/home/$LOGINUSR/Desktop/rome.desktop <<EOXAPE
[Desktop Entry]
Version=1.0
Type=Application
Comment=ROME [ROM] [E]xtractor
Terminal=false
Name=ROME
Exec=/home/$LOGINUSR/programs/ROME/rome
Icon=/home/$LOGINUSR/programs/ROME/icons/rome_icon.png
EOXAPE
chmod +x /home/$LOGINUSR/Desktop/rome.desktop

# mAid installer
echo -e "\nmAid installer:"
cat > /home/$LOGINUSR/Desktop/install-maid.desktop <<EODESK
[Desktop Entry]
Version=1.0
Type=Application
Comment=Flash mAid to your disk
Terminal=false
Name=Install mAid
Exec=sudo /home/$LOGINUSR/.fwul/install-disk.sh
Icon=/home/$LOGINUSR/.fwul/mAid_installer_icon.png
EODESK
chmod +x /home/$LOGINUSR/Desktop/install-maid.desktop


# install mAid installer
echo -e "\nINSTALL INSTALLER REPO:\n"
[ -d /etc/calamares ] && rm -rf /etc/calamares
[ -d $TMP_PKGDIR/installer ] && rm -rf $TMP_PKGDIR/installer
case "$MAID_TYPE" in
    stable|nightly) MIBR=$MAID_TYPE ;;
    *) MIBR=nightly ;;
esac
F_GITCLONE https://code.binbash.rocks:8443/mAid/maid_installer.git $TMP_PKGDIR/installer \
    && cd $TMP_PKGDIR/installer \
    && git checkout $MIBR \
    && mv calamares /etc/
rm -rf $TMP_PKGDIR/installer
cd /

#########################################################################################
#########################################################################################

# ensure we can talk with dbus here
echo -e "\n... exporting dbus session"
export $(dbus-launch)

# ensure proper perms on home dir
chown -R $LOGINUSR /home/$LOGINUSR/Desktop/

# enable services
systemctl enable pacman-init.service # without dependency to gnupg mount in RAM
systemctl enable choose-mirror.service
systemctl set-default graphical.target
systemctl enable systemd-networkd
systemctl enable NetworkManager
systemctl enable init-mirror
systemctl enable bluetooth
#SEE UPSTREAM SECTION: systemctl enable vboxservice
systemctl enable fwul-session

# disable services
systemctl disable systemd-networkd-wait-online.service # fix error msg on boot and works only with -i <interface name> anyways -> bug??

# Create a MD5 for a given file or set to 0 if file is missing
F_DOMD5(){
    MFILE="$1"
    [ -z "$MFILE" ]&& echo "MISSING ARG FOR $FUNCNAME!" && exit 3
    if [ -f "$MFILE" ];then
        md5sum $MFILE | cut -d " " -f 1
    else
        echo 0
    fi
}

# wait until a file gets written or modified
F_FILEWAIT(){
    MD5C=$1
    FILE="$2"

    [ -z "$FILE" -o -z "$MD5C" ] && echo "MISSING ARG FOR $FUNCNAME!" && exit 3
    while true; do
        MD5NOW=$(F_DOMD5 "$FILE")
        if [ "$MD5C" != "$MD5NOW" ];then
            break
        else
            echo -e "\t.. waiting that the changes gets written for $FILE..\n\t($MD5C vs $MD5NOW)"
            sleep 1s
        fi
    done
}

# compile dconf db's
echo -e "\n... compiling mAid dconf db"
dbus-launch dconf compile /etc/dconf/db/maid /etc/dconf/db/database.d

# set aliases
echo -e '\n# mAid aliases\nalias fastboot="sudo fastboot"\n' >> /home/$LOGINUSR/.bashrc

# install additional firmware
pacman -Q bcm4350-firmware || su -c - $LOGINUSR "yay -S --noconfirm bcm4350-firmware"
pacman -Q zd1211-firmware || su -c - $LOGINUSR "yay -S --noconfirm zd1211-firmware"

# ensure proper timesync
systemctl enable systemd-timesyncd

# ensure proper perms (have to be done BEFORE using any su LOGINUSER cmd which writes to home dir!)
for p in $(find /home/$LOGINUSR/ -maxdepth 1 -mindepth 1 -type d ! -name '.*gvfs' 2> /dev/null);do
    chown -R ${LOGINUSR}:users $p
done

# install mAid desktop package + add to repo
echo -e "\nINSTALL MAID DESKTOP PKG" 
#for size in light full;do
  #  [ -d $TMP_PKGDIR/md ] && rm -rf $TMP_PKGDIR/md
  #  mkdir $TMP_PKGDIR/md
 #   F_GITCLONE https://code.binbash.rocks:8443/mAid/mAid-desktop.git -b $size $TMP_PKGDIR/md \
  #      && cd $TMP_PKGDIR/md \
   #     && chown $LOGINUSR $TMP_PKGDIR/md \
  #      && su -c - $LOGINUSR "makepkg -s --noconfirm" \
   #     && pacman --noconfirm -U maid-desktop-${size}*.pkg.tar.zst \
   #     && cp maid-desktop-${size}*.pkg.tar.zst $REPODIR/
  #  cd / 
#done
#rm -rf $TMP_PKGDIR/md

###############################################################################################################
#
# UPSTREAM FIX REQUIRED - WORKAROUND SECTION
#
###############################################################################################################

# ensure we do not remove the module when removing kernel headers
find /usr/src -name dkms.conf -exec mv -v {} {}.disabled \;

systemctl enable vboxservice
# vbox utils bug - end
########################

###############################################################################################################
#
# cleanup
#
###############################################################################################################

# copy minimal needed noto fonts
if [ ! -d /usr/share/fonts/noto-minimal ];then
    mkdir /usr/share/fonts/noto-minimal
    cp /usr/share/fonts/noto/NotoSansMono-* /usr/share/fonts/noto-minimal/
    cp /usr/share/fonts/noto/NotoSans-* /usr/share/fonts/noto-minimal/
fi

# NOTE: smbclient is required for budgie-control-center to start
echo -e "\nCleanup - pacman:"
IGNPKG="adwaita-icon-theme man-db man-pages mdadm nano netctl openresolv pcmciautils reiserfsprogs s-nail vi zsh memtest86+ caribou gnome-backgrounds gnome-themes-extra gnome-themes-standard nemo telepathy-glib zeitgeist gnome-icon-theme progsreiserfs linux316 linux316-virtualbox-guest-modules qt5-tools ruby rubygems geoip-database-extra ${MAID_KERNEL}-headers acpica bin86 cdrtools dev86 glu gsoap java-environment-common jdk7-openjdk jre7-openjdk jre7-openjdk-headless lib32-glibc libidl2 libstdc++5 libvncserver sdl sdl_ttf vde2 xalan-c xerces-c xorg-server-devel xorg-util-macros lib32-gcc-libs noto-fonts archlinux-appstream-data groff virtualbox-guest-dkms flex xfce4-panel garcon cdparanoia docbook-xml docbook-xsl jack2 gcc"

if [ "$MAID_SIZE" == "light" ];then
    for igpkg in $IGNPKG;do
        PFOUND=1
        pacman -Q $igpkg || PFOUND=0 2>&1 >> /dev/null
        [ $PFOUND -eq 1 ] && echo "trying to remove $igpkg as it seems to be installed.." && pacman --noconfirm -Rns -dd $igpkg
        [ $PFOUND -eq 0 ] && echo "PACMAN: package $igpkg not found for removal"
        true
    done
fi

echo -e "\nCleanup - python stuff:"
for pydir in "/usr/lib/python2.*" "/usr/lib/python3.*";do
    echo "deleting test dir for $pydir"
    rm -rf $pydir/test/*
    echo "deleting pyo's,pyc's & pycaches in $pydir"
    find "$pydir" -type f -name "*.py[co]" -delete -print || echo "nothing to clean in $pydir"
    find "$pydir" -type d -name "__pycache__" -delete -print 2>/dev/null || echo "nothing to clean in $pydir"
done

if [ "$MAID_SIZE" == "light" ];then
    echo -e "\nCleanup - folders:"
    DELFOLD="/usr/share/icons/HighContrast /usr/share/icons/hicolor /usr/share/icons/locolor /share/icons/Adwaita \
            /usr/share/icons/gnome /usr/share/icons/Numix-Light /usr/share/icons/gnome \
            /usr/share/icons/ePapirus /usr/share/icons/Sea /usr/share/icons/Papirus-Dark /usr/share/icons/Papirus-Light \
            /usr/share/icons/Bibata-Original-Pink /usr/share/icons/Bibata-Original-DodgerBlue /usr/share/icons/Bibata-Original-DarkRed \
            /usr/share/icons/Bibata-Modern-Pink /usr/share/icons/Bibata-Original-Turquoise /usr/share/icons/Bibata-Modern-DodgerBlue /usr/share/icons/Bibata-Modern-DarkRed \
            /home/$LOGINUSR/programs/lglafsploit/.git/objects /home/$LOGINUSR/.cache /var/tmp /var/cache/pacman"
    for delf in $DELFOLD;do
        [ -d "$delf" ] && rm -rf "$delf"
        true
    done
fi

if [ "$MAID_SIZE" == "light" ];then
    # fix missing icons in budgie-control-center (as the icons will land in hicolor paths)
    pacman -S --noconfirm budgie-control-center hicolor-icon-theme
fi

echo -e "\nCleanup - files:"
DELFILES="/livefs-pkgs.txt /rootfs-pkgs.txt"
for delfi in $DELFILES;do
    [ -f "$delfi" ] && rm -f "$delfi"
    true
done

echo -e "\nCleanup - pacman orphans:"
PMERR=$(pacman --noconfirm -Rns $(pacman -Qtdq) || echo no pacman orphans)
echo -e "\nCleanup - yay orphans:"
YERR=$(su -c - $LOGINUSR "yay -Qtd --noconfirm" || echo no yay orphans)

if [ "$MAID_SIZE" == "light" ];then
    echo -e "\nCleanup - manpages:"
    rm -rf /usr/share/man/*

    echo -e "\nCleanup - docs:"
    rm -rf /usr/share/doc/* /usr/share/gtk-doc/html/*
fi

echo -e "\nCleanup - misc:"
rm -rf /*.tgz /*.tar.gz /yay/ /package-query/ /home/$LOGINUSR/.fwul/tmp/* /usr/src/*

echo -e "\nCleanup - archiso:"
rm -rf /etc/fwul

#echo -e "\nCleanup - linux firmware:"
#rm -rf /usr/lib/firmware/netronome /usr/lib/firmware/liquidio /usr/lib/firmware/mellanox

echo -e "\nCleanup - gradle"
rm -rf /home/$LOGINUSR/.gradle

echo -e "\nCleanup - bleachbit"
[ ! -d /root/.config/bleachbit/ ] && mkdir -p /root/.config/bleachbit
grep -q "[preserve_languages]" /root/.config/bleachbit/bleachbit.ini || echo -e "\n[preserve_languages]" >> /root/.config/bleachbit/bleachbit.ini
for l in $(cut -d "." -f1 /etc/locale.gen);do
    grep -q "$l = True" /root/.config/bleachbit/bleachbit.ini || sed -i "/\[preserve_languages\]/a $l = True" /root/.config/bleachbit/bleachbit.ini
    grep -q "${l/_*} = True" /root/.config/bleachbit/bleachbit.ini || sed -i "/\[preserve_languages\]/a ${l/_*} = True" /root/.config/bleachbit/bleachbit.ini
done
pacman -Q bleachbit || pacman -S --noconfirm bleachbit 
#bleachbit --clean system.localizations >> /dev/null
#bleachbit --clean bash.history journald.clean >> /dev/null
#su -c - $LOGINUSR "bleachbit --clean bash.history"
pacman --noconfirm -Rnsc bleachbit

echo -e "\nCleanup - ibus"
[ -d "/tmp/dicts" ] && rm -rf /tmp/dicts
mkdir /tmp/dicts
for l in $(cut -d "." -f1 /etc/locale.gen);do
    cp /usr/share/ibus/dicts/emoji-${l/_*}.dict /tmp/dicts/
done
rm -rf /usr/share/ibus/dicts/emoji*
mv /tmp/dicts/* /usr/share/ibus/dicts/

# persistent perms for fwul
cat > $RSUDOERS <<EOSUDOERS
%wheel     ALL=(ALL) ALL

# special rules for session & language
%wheel     ALL=(ALL) NOPASSWD: /bin/mount -o remount\,size=* /run/archiso/cowspace
%wheel     ALL=(ALL) NOPASSWD: /bin/umount -l /tmp
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /var/tmp/* /tmp/
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /tmp/locale.conf /etc/locale.conf
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /tmp/environment /etc/environment
# needed for language workaround
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/systemctl restart lightdm

# let the user sync the databases without asking for pw
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/yay --noconfirm -Sy
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman --noconfirm -Sy
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman -Sy --noconfirm
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman -Sy

# let the user install deps and compiled packages by yay without asking for pw
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman *.pkg.tar.* -Ud *
%wheel     ALL=(ALL) NOPASSWD: /bin/cp -vf /home/$LOGINUSR/.cache/yay/*/*.pkg.tar.* /var/cache/pacman/*

# when using fwul package installer no pw 
%wheel     ALL=(ALL) NOPASSWD: /home/$LOGINUSR/.fwul/install_package.sh *

# special rules for TeamViewer
%wheel     ALL=(ALL) NOPASSWD: /bin/systemctl start teamviewerd
%wheel     ALL=(ALL) NOPASSWD: /bin/systemctl enable teamviewerd

# allow ANY pacman installation without prompting
# (DO NOT TOUCH THE ABOVE COMMENT LINE! IT'S USED AS A FILTER DURING INSTALL)
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman * -U *
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman -U *
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman * -S *
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/pacman -S *

# special rule for Sony Flashtool
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/yay --noconfirm -S xperia-flashtool
%wheel     ALL=(ALL) NOPASSWD: /bin/cp x10flasher.jar /usr/lib/xperia-flashtool/

# special rule for SP Flashtool
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/yay --noconfirm -S spflashtool-bin

# special rule for fastboot
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/fastboot *

# SALT
%wheel     ALL=(ALL) NOPASSWD: /home/$LOGINUSR/programs/SALT/salt

# mAid mode
%wheel     ALL=(ALL) NOPASSWD: /bin/mv /tmp/maid-release /etc/maid-release

# qphotorec
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/qphotorec *

# mAid installer
%wheel     ALL=(ALL) NOPASSWD: /home/$LOGINUSR/.fwul/install-disk.sh

# set java version (for JOdin legacy)
%wheel     ALL=(ALL) NOPASSWD: /usr/bin/archlinux-java set *
EOSUDOERS

# set root password
passwd root <<EOSETPWROOTPW
$RPW
$RPW
EOSETPWROOTPW

# set real release info
echo "maidversion=${MAID_VERSION}" > /etc/maid-release
echo "maidversiontype=${MAID_TYPE}" >> /etc/maid-release
if [ "$MAID_TYPE" != "stable" ];then
    echo "maidtype=nightly" >> /etc/maid-release
else
    echo "maidtype=stable" >> /etc/maid-release
fi
echo "maidbuild=$(date +%s)" >> /etc/maid-release
echo "patchlevel=0" >> /etc/maid-release
echo "maidsize=$MAID_SIZE" >> /etc/maid-release
ln -sf /etc/maid-release /etc/fwul-release

# media fix
[ ! -d /media ] && mkdir /media
chmod 755 /media

# etc fix
chown -R root:root /etc

# set default user for lightdm
test -d /var/lib/lightdm/.cache/lightdm-gtk-greeter || mkdir -p /var/lib/lightdm/.cache/lightdm-gtk-greeter/
echo -e "[greeter]\nlast-user=$LOGINUSR" > /var/lib/lightdm/.cache/lightdm-gtk-greeter/state && chmod 755 /var/lib/lightdm/.cache/lightdm-gtk-greeter/state

# licensing, copyright for mAid scripts
echo "... updating copyright"
curyear=$(date +%Y)
for f in $(find /etc /home/$LOGINUSR/ -type f -name '*.sh' 2> /dev/null);do
    grep -qE "MAIDCOPYRIGHT" $f && sed -i "s/@@MAIDCOPYRIGHT@@/2017-${curyear} steadfasterX <steadfasterX | at | gmail - com>/g" $f && echo "...  |- $f changed"
    grep -qE "MAIDLICENSE" $f && sed -i 's#@@MAIDLICENSE@@#LGPLv3 - a copy can be found here: /usr/share/licenses/mAid#g' $f && echo "...  |- $f changed"
done

# backup gnupg setup to avoid issues when pacman-init systemd service fails
# fallback only - no automatism added to apply it later
cd /etc/pacman.d && tar czvf /root/pacman.gnupg.tgz gnupg; cd /

# ensure hosts does not contain build stuff
mv /etc/hosts.orig /etc/hosts
########################################################################################
# TEST AREA - TEST AREA - TEST AREA 

echo -e "\nTESTING MAID BUILD!"

# arch independent requirements
REQFILES="/home/$LOGINUSR/.fwul/wallpaper_fwul.png 
$RSUDOERS
/home/$LOGINUSR/Desktop/LG/open-lglafshell.desktop
/home/$LOGINUSR/Desktop/LG/open-lglafng.desktop
/home/$LOGINUSR/programs/SALT/salt
/home/$LOGINUSR/programs/lglafng/partitions.py
/home/$LOGINUSR/programs/lglaf/partitions.py
/home/$LOGINUSR/programs/kdztools/unkdz.py
/home/$LOGINUSR/Desktop/LG/SALT.desktop
/home/$LOGINUSR/Desktop/ADB.desktop
/home/$LOGINUSR/programs/sadb/starter.sh
/home/$LOGINUSR/programs/sadb/S-ADB.jar
/home/$LOGINUSR/Desktop/Samsung/heimdall.desktop
/home/$LOGINUSR/Desktop/install-TV.desktop
/usr/bin/adb
/usr/bin/fastboot
/usr/bin/heimdall
/usr/bin/yay
/usr/bin/qphotorec
/home/$LOGINUSR/Desktop/qphotorec.desktop
/home/$LOGINUSR/.fwul/odin-logo.jpg
/home/$LOGINUSR/.fwul/install_spflash.sh
/home/$LOGINUSR/.fwul/install_sonyflash.sh
/home/$LOGINUSR/Desktop/install-sonyflash.desktop
/home/$LOGINUSR/.android/adb_usb.ini
/etc/udev/rules.d/51-android.rules
/home/$LOGINUSR/programs/welcome/welcome.sh
/home/$LOGINUSR/programs/welcome/icons/welcome.png
/home/$LOGINUSR/.config/autostart/welcome.desktop
/home/$LOGINUSR/.config/autostart/z_setlockbg.desktop
/etc/systemd/scripts/fwul-session.sh
/etc/profile.d/maid-language.sh
/home/$LOGINUSR/.xprofile
/home/$LOGINUSR/.fwul/maid-session.sh
/etc/systemd/system/init-mirror.service
/etc/systemd/scripts/init-fwul
/home/$LOGINUSR/Desktop/welcome.desktop
/etc/maid-release
/usr/local/bin/livepatcher.sh
/usr/local/bin/liveupdater.sh
/var/lib/fwul/generic.vars
/var/lib/fwul/generic.func
/home/$LOGINUSR/.fwul/sshkeygen.sh
/home/$LOGINUSR/.config/autostart/sshkeygen.desktop
/home/$LOGINUSR/.fwul/pkexecgui
/home/$LOGINUSR/Desktop/tmate.desktop
/home/$LOGINUSR/.fwul/tmate.sh
/home/$LOGINUSR/.fwul/tmate-logo.png
/home/$LOGINUSR/Desktop/Xiaomi/miflash.desktop
/home/$LOGINUSR/programs/MiFlash/README.md
/home/$LOGINUSR/programs/MiFlash/Xiaomi.png
/home/$LOGINUSR/programs/MiFlash/starter.sh
/home/$LOGINUSR/programs/ROME/rome
/home/$LOGINUSR/Desktop/rome.desktop
/etc/lightdm/lightdm.conf
/etc/dconf/profile/user
/etc/dconf/db/database.d/maid
/etc/pacman.ignore
/etc/dconf/db/maid
/home/$LOGINUSR/Desktop/install-chromium.desktop
/home/$LOGINUSR/.fwul/install_chromium.sh
/home/$LOGINUSR/Desktop/install-ff.desktop
/home/$LOGINUSR/.fwul/install_ff.sh
/home/$LOGINUSR/.fwul/install-disk.sh
/home/$LOGINUSR/Desktop/install-maid.desktop
/etc/pacman.locale
/home/$LOGINUSR/Desktop/support.desktop
/etc/mAid/maid.vars
/etc/mAid/repo.gpg"
#$REPODIR/spflashtool*.tar.zst
#$REPODIR/chromium*.tar.zst
#$REPODIR/xperia*.tar.zst
#$REPODIR/teamviewer*.tar.zst
#$REPODIR/easy-installer*.tar.zst
#$REPODIR/firefox*.tar.zst
#/etc/calamares/branding/maid/branding.desc
#/usr/share/licenses/mAid/LICENSE"

# 32bit requirements (extend with 32bit ONLY.
# If the test is the same for both arch use REQFILES instead)
REQFILES_i686="$REQFILES"

# 64bit requirements (extend with 64bit ONLY. 
# If the test is the same for both arch use REQFILES instead)
REQFILES_x86_64="$REQFILES
/home/$LOGINUSR/Desktop/install-spflash.desktop
/home/$LOGINUSR/Desktop/Samsung/JOdin.desktop
/home/$LOGINUSR/programs/JOdin/starter.sh
/home/$LOGINUSR/programs/JOdin/JOdin3CASUAL
/home/$LOGINUSR/.fwul/install_jodin3.sh
/home/$LOGINUSR/Desktop/Samsung/install-jodin3-2021.desktop"

CURREQ="REQFILES_${arch}"

for req in $(echo -e "${!CURREQ}"|tr "\n" " ");do
    if [ -f "$req" ];then
        echo -e "\t... testing ($arch): $req --> OK"
    else
        echo -e "\t******************************************************************************"
        echo -e "\tERROR: testing ($arch) $req --> FAILED!!"
        echo -e "\t******************************************************************************"
        exit 3
    fi
done

# add a warning when debugging is enabled
if [ "$DEBUG" -eq 1 ];then
        echo -e "\t******************************************************************************"
        echo -e "\tWARNING: DEBUG MODE ENABLED !!!"
        echo -e "\t******************************************************************************"
fi

# TEST AREA - END
########################################################################################

# list the biggest packages installed
pacman --noconfirm -S expac
expac -H M -s "%-30n %m" | sort -rhk 2 | head -n 40
pacman --noconfirm -Rns expac

# create a XDA copy template for the important package versions
echo -ne '* **Versions of the main mAid components:**\n   * Kernel -> **'version:$(pacman -Qi $MAID_KERNEL |grep Version| cut -d ":" -f 2)'**\n   * ADB and fastboot: '
pacman -Q android-tools | sed 's/ / -> **version: /g;s/$/**/g'
echo -e '   * simple-adb GUI -> **version: XXXXXXXXXXXXX**'
echo -e '   * SALT -> **version: '$(grep -E -o 'VDIG=.*' /home/$LOGINUSR/programs/SALT/salt.vars | cut -d '"' -f2)'**'
echo -e '   * ROME -> **version: '$(grep -E -o 'VDIG=.*' /home/$LOGINUSR/programs/ROME/rome.vars | cut -d '"' -f2)'**'
echo -e "   * Firefox (bundled) -> **version: ${FFVERS}**"
echo -e "   * JOdin3 2021 (installer pre-package) -> **version: ${JDVERS}**"
echo -e "   * Java8 (installer pre-package) -> **version: ${JREVERS}**"
echo -e "   * Easy Installer (installer pre-package) -> **version: ${EIVERS}**"
echo -e "   * TeamViewer (installer pre-package) -> **version: ${TVVERS}**"
echo -e "   * SonyFlash tools (installer pre-packaged) -> **version: ${SONYVERS}**"
echo -e "   * SPFlash tools (installer pre-packaged) -> **version: ${SPFTVERS}**"
echo -e "   * Chromium (installer pre-packaged) -> **version: ${CHRVERS}**"
CHLOG="bootimgtool-git heimdall-git budgie-desktop lightdm xorg-server virtualbox-guest-utils testdisk-wip tmate"
for i in $CHLOG;do
        echo "   * $(pacman -Q $i | sed 's/ / -> **version: /g;s/$/**/g')"
done

F_CLEANPKG

# generate a package list (disabled as we do not mount any repo anymore)
#pacman -Qq > $REPODIR/packages_${MAID_SIZE}.txt

# cleanup sig db
rm /var/lib/pacman/sync/mAid*.sig || true

# fix perms
# https://code.binbash.it:8443/FWUL/build_fwul/issues/58
chown -v root:root /usr
chmod -v 755 /usr
chown -v root:root /usr/share
chmod -v 755 /usr/share

#########################################################################################
# this has always to be the very last thing!
rm -vf $TMPSUDOERS
