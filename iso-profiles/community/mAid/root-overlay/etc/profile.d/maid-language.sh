#!/bin/bash
###########################################################################################
#
# X and terminal session starter for mAid 
#
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    @@MAIDCOPYRIGHT@@
# License:      @@MAIDLICENSE@@
#
###########################################################################################

######
# Language & Locale
LANGFILE="REPLACEHOME/.dmrc"

# set system language and keyboard layout
if [ -f "$LANGFILE" ];then
    # extract, fix and export $LANG
    TEMPLANG=$(cat "$LANGFILE" | grep ^Language= | cut -d '=' -f 2 | sed 's/utf8/UTF8/')
    [ ! -z "$TEMPLANG" ] && export LANG=$TEMPLANG

    # convert $LANG to $LANGUAGE and export
    SETLAYOUT="$(echo $LANG | cut -d '@' -f 1 | cut -d '.' -f 1 | cut -d '_' -f 1)"

    # special layout adaptions ..
    case "$SETLAYOUT" in
        en) SETLAYOUT="us" ;;
        de) SETLAYOUT="de+nodeadkeys" ;;
        *) SETLAYOUT="${SETLAYOUT}"
    esac

    # set the keyboard layout (if not root)
    [ $(id -u) != 0 ] && dconf write /org/gnome/desktop/input-sources/sources "[('xkb', '$SETLAYOUT')]"

    # set global system locale
    DMLANG=$(cat "$LANGFILE" | grep ^Language= | cut -d '=' -f 2 | sed 's/utf8/UTF-8/')
    if [ -z "$DMLANG" -o $? -ne 0 ];then
        echo "$0: ERROR setting language by $LANGFILE !!"
    else
        echo "LANG=\"$DMLANG\"" > /tmp/locale.conf
        # set fallback language
        echo "LANGUAGE=\"en_US.UTF-8\"" >> /tmp/locale.conf
        sudo mv /tmp/locale.conf /etc/locale.conf

        # ensure PAM locale is set, see issue #88. 
        # This also fixes an issue where the language does not change in the greeter top right after logging out
        grep -v "LANG=" /etc/environment > /tmp/environment
        echo "LANG=\"$DMLANG\"" >> /tmp/environment
        sudo mv /tmp/environment /etc/environment
    fi
fi

# detect mode
grep -v "maid_" /etc/maid-release > /tmp/maid-release
hostnamectl status | strings |grep -v ID |tr -d " " | tr ":" "=" |sed 's/^/maid_/g' >> /tmp/maid-release
grep maidversion /tmp/maid-release >> /dev/null && sudo mv /tmp/maid-release /etc/maid-release
