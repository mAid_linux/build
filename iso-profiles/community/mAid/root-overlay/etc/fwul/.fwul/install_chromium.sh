#!/bin/bash
##########################################################################
#
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    @@MAIDCOPYRIGHT@@
# License:      @@MAIDLICENSE@@
#
##########################################################################

source /etc/maid-release
source /etc/mAid/maid.vars

pacman -Q chromium || sudo $HOME/.fwul/install_package.sh pacman chromium
pacman -Q chromium && cp /usr/share/applications/chromium.desktop ~/Desktop/ && rm ~/Desktop/install-chromium.desktop && chmod +x ~/Desktop/chromium.desktop
