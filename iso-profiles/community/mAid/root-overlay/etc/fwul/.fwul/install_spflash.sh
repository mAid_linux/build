#!/bin/bash
##########################################################################
#
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    @@MAIDCOPYRIGHT@@
# License:      @@MAIDLICENSE@@
#
##########################################################################

source /etc/maid-release
source /etc/mAid/maid.vars

pacman -Q spflashtool-bin || sudo $HOME/.fwul/install_package.sh pacman spflashtool-bin
pacman -Q spflashtool-bin && cp /usr/share/applications/spflashtool.desktop ~/Desktop/ && chmod +x ~/Desktop/spflashtool.desktop  && rm ~/Desktop/install-spflash.desktop

