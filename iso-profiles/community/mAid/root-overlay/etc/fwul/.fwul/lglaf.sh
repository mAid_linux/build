#!/bin/bash
###########################################################################################
#
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    @@MAIDCOPYRIGHT@@
# License:      @@MAIDLICENSE@@
#
###########################################################################################

args="$@"
yad --title="LG-LAF" --center --width=500 --height=100 --nobuttons --button=Abort:99 --button=Continue:1 --text "\n\n\tEnter Download Mode on your LG and connect it to this PC.\n\tThen click on continue.\n\n"
[ $? -eq 1 ] && gnome-terminal -- bash -c "cd $HOME/programs/lglaf; /usr/bin/python2 lglaf.py $args"
