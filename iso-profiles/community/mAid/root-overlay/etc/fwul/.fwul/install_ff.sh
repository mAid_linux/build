#!/bin/bash
##########################################################################
#
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    @@MAIDCOPYRIGHT@@
# License:      @@MAIDLICENSE@@
#
##########################################################################

source /etc/maid-release
source /etc/mAid/maid.vars

pacman -Q firefox || sudo $HOME/.fwul/install_package.sh pacman firefox
pacman -Q firefox && cp /usr/share/applications/firefox.desktop ~/Desktop/ && rm ~/Desktop/install-ff.desktop && chmod +x ~/Desktop/firefox.desktop
