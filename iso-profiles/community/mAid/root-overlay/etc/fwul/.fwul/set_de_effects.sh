#!/bin/bash
###############################################################################################################
#
# Manage Deepin Desktop Effects
#
###############################################################################################################

YBIN=/usr/bin/yad

[ ! -d ~/.config/deepin/deepin-wm-switcher/ ] && mkdir -p ~/.config/deepin/deepin-wm-switcher/ && chown -R android ~/.config/deepin/deepin-wm-switcher

YAD="$YBIN --center --top --title=Manage_Desktop_Effects --window-icon=input-gaming"

$YAD --image=input-gaming --text-align=center --width 380 --height 50 --form --text "\nEnable or Disable Desktop effects.\n\nEnabling effects may decrease system performance on low-end systems.\n" --button="Disable":1 --button="Enable":2 --button="Cancel":0
ANS=$?
echo "answer was: $ANS"
# do the magic
case $ANS in
    1)
        echo '{"last_wm":"deepin-metacity","wait":true}' > ~/.config/deepin/deepin-wm-switcher/config.json
        nohup deepin-metacity --replace >> /dev/null 2>&1 &
    ;;
    2)
        echo '{"last_wm":"deepin-wm","wait":true}' > ~/.config/deepin/deepin-wm-switcher/config.json
        nohup deepin-wm --replace >> /dev/null 2>&1 &
    ;;
    *) exit $? ;;
esac

