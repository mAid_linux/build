#!/bin/bash
###########################################################################################
#
# X and terminal session starter for mAid 
#
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    @@MAIDCOPYRIGHT@@
# License:      @@MAIDLICENSE@@
#
###########################################################################################

source /etc/maid-release
source /etc/mAid/maid.vars

######
# initial setup detection file
FIRSTRUN="REPLACEHOME/.fwul/skipsetup"

######
# make all desktop files usable:
# chmod'ing +x was enough long time ago,
# then gio need to be used to set metadata trusted = true and
# now we require a checksum.. let's see what comes next, maybe an email confirmation + phone call..?! :/
if [ ! -f "$FIRSTRUN" ];then
    echo -e "\n... setting desktop files trust"
    for f in $(find REPLACEHOME/Desktop -name '*.desktop'); do
        chmod +x "$f"
        gio set -t string "$f" metadata::xfce-exe-checksum "$(sha256sum $f | awk '{print $1}')"
    done
    if [ "$maidsize" == "full" ];then
        for sh in $(find REPLACEHOME/.fwul/ -type f -name 'install_*.sh' | grep -v install_package);do
            bash $sh
        done
    fi
fi

# initial setup when needed
if [ ! -f "$FIRSTRUN" ];then
    # mark as finished
    touch $FIRSTRUN
fi
