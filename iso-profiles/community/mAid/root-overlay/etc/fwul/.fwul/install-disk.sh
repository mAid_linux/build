#!/bin/bash
###############################################################################################################
#
# Install mAid to your disk
#
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    @@MAIDCOPYRIGHT@@
# License:      @@MAIDLICENSE@@
#
###############################################################################################################

source /etc/maid-release
source /etc/mAid/maid.vars

DEBUG=0
YBIN=/usr/bin/yad
PMANEXEC=pacman
LOGDIR=/var/log/maid
LOG=${LOGDIR}/install-maid.log
YICON=/home/android/.fwul/mAid_installer_icon.png
YAD="$YBIN --center --top --title=mAid_Installer --window-icon=$YICON"
PKGS="calamares rsync mkinitcpio squashfs-tools gnu-free-fonts os-prober"

[ ! -f "$YBIN" ]&& echo "missing yad dependency! ABORT" && read -p "PRESS ANY KEY TO QUIT" DUMMY && exit

[ ! -d "$LOGDIR" ] && mkdir -p $LOGDIR
sudo chmod -R 666 $LOGDIR
echo "starting new log at $(date)" > $LOG

F_EXIT(){
    ECD=$1
    exit $ECD
}

$YAD --image=$YICON --text-align=center --width 600 --height 200 --form --text "\n\nContinuing will flash <b>Manjaro (mAid edition)</b>\nto your local disk or USB device!\n\n(this requires a working Internet connection)\n\nClick <a href='$WIKIURL#full-installation'>here</a> for the installation guides\n"

# do the magic
if [ $? -eq 0 ];then
    # umounting vbox sharedfolders (if any) is needed to allow copying $HOME later
    umount -a -t vboxsf 2>&1 | tee -a $LOG | stdbuf -i0 -o0 -e0 sed 's/^/# /g' | $YAD --width=300 --height=400 --progress --percentage=10 --auto-close --pulsate --enable-log --enable-log="umnounting shared folders ..."

    [ -d /tmp/maid ] && rm -rf /tmp/maid
    [ -d /etc/calamares ] && rm -rf /etc/calamares
    git clone $GITURL/maid_installer.git /tmp/maid >> $LOG 2>&1 \
        && cd /tmp/maid \
        && git checkout $maidtype >> $LOG 2>&1 \
        && mv -v /tmp/maid/calamares /etc/ >> $LOG 2>&1
    if [ $? -ne 0 ];then
        echo "error checking out installer sources!" >> $LOG
        $YAD --image=script-error --width 600 --height 800 --text "\n\tERROR occured while preparing the installer!\n\tCannot continue..\n" --text-info --filename="$LOG" --button=Close
        F_EXIT 3
    fi

    $PMANEXEC -Qq calamares || sudo /home/$SUDO_USER/.fwul/install_package.sh pre-pkg maidsetup
    
    # re-check pre-requirements and installer pkg - this is just meant as a FALLBACK as all these come with the bundled installer already!
    for PKG in $PKGS;do
        $PMANEXEC -Qq $PKG
        if [ $? -ne 0 ];then
            $PMANEXEC -S --needed --noconfirm $PKG 2>&1 | tee -a $LOG | stdbuf -i0 -o0 -e0 sed 's/^/# /g'| $YAD --width=600 --height=400 --progress --enable-log="installing: $PKG ..." --auto-close --log-expanded --auto-kill --button=Abort:9 --pulsate
            if [ ${PIPESTATUS[0]} -ne 0 ];then 
                $YAD --image=script-error --width 600 --height 800 --text "\n\tERROR occured while installing:\n\t$PKG\n" --text-info --filename="$LOG" --button=Close
                F_EXIT 3
            fi
        fi
    done

    # even though the documentation showing a way to use an udev rule which
    # does not auto-mount USB devices while keeping it visible to the user
    # whatever I tried it was not possible to get it working as documented.
    # so we use the hammer method instead which completely removes auto-mounted
    # USB and internal devices.
    # e.g. this udev rule should do what I want: not auto-mounting USB devices 
    # while offering them in the file manager for manual mounting by a click 
    # (as it is with internal filesystems)
    #       ACTION=="add|bind|change", SUBSYSTEM=="block", KERNEL=="sd[b-e]*", ENV{UDISKS_AUTO}="0", ENV{UDISKS_AUTOMOUNT_HINT}="never"
    # UDISKS_IGNORE=1 would work but it would any USB device inaccessible in
    # the file explorer until mounted manually (not what we want for the avg
    # mAid user).
    # UDISKS_SYSTEM_INTERNAL=1 should treat them as an internal device (so no
    # automount) but even that gets ignored and USB devices still getting mounted.
    #
    # this handling is needed (no matter which method) to ensure the installer
    # can fully access e.g. an USB stick to flash mAid there. Otherwise the user
    # would need to do a manual partitioning which is not intended for mAid.
    # stopping udisks2 will unmount all internal and external filesystems (i.e.
    # USB) and as there is no need for the installer to access internal
    # filesystems we can use that one instead of adding a udev rule and
    # activating that before starting the installer.
    systemctl stop udisks2

    # this one is needed after mAid has been installed, it will be loaded in 
    # the live env only
    modprobe squashfs

    # start the installer
    [ $DEBUG == 1 ] && calamares -d >> $LOG 2>&1
    [ $DEBUG == 0 ] && calamares >> $LOG 2>&1
else
    echo "aborted by user $(date)" >> $LOG
fi
