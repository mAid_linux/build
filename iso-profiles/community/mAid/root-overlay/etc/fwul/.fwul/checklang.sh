#!/bin/bash
####################################################################################
#
# sets and checks for the language and offers a logout including a lightdm restart
# when needed
#
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    @@MAIDCOPYRIGHT@@
# License:      @@MAIDLICENSE@@
#
####################################################################################

source "/etc/profile.d/fwul-session.sh"

LANGFILE="REPLACEHOME/.dmrc"
DMLANG=$(cat "$LANGFILE" | grep ^Language= | cut -d '=' -f 2 | tr '[:upper:]' '[:lower:]' | tr -d "-")
WLANG=$(echo "$LANG" | tr '[:upper:]' '[:lower:]' | tr -d "-")

if [ "$WLANG" != "$DMLANG" ];then
    yad --title="Fix the language" --fixed --height=200 --width=600 --image="REPLACEHOME/.fwul/warning_64x64.png"  --text "\n   Oops.. It seems you have changed the desktop language ($LANG vs $DMLANG).\n\n   To fully apply the change you need to login again by clicking the\n\n   <b>\"Apply &amp; Logout\"</b>\n\n   button below\n\n   (this is a workaround for rare situations)" --button="Apply &amp; Logout":2
        sudo systemctl restart lightdm
fi
