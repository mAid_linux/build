#!/bin/bash
##########################################################################
#
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    @@MAIDCOPYRIGHT@@
# License:      @@MAIDLICENSE@@
#
##########################################################################

source /etc/maid-release
source /etc/mAid/maid.vars

DSKFILE="$HOME/Desktop/Samsung/JOdin3-2021.desktop"

pacman -Q jodin3-bin || sudo $HOME/.fwul/install_package.sh pacman jodin3-bin
pacman -Q jodin3-bin && cp /usr/share/applications/jodin3.desktop $DSKFILE && rm ~/Desktop/Samsung/install-jodin3-2021.desktop && chmod +x "$DSKFILE" && gio set -t string "$DSKFILE" metadata::xfce-exe-checksum "$(sha256sum "$DSKFILE" | awk '{print $1}')"
