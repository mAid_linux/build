#!/bin/bash
########################################################
# Simple convenient tmate starter
#
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    @@MAIDCOPYRIGHT@@
# License:      @@MAIDLICENSE@@
#
########################################################

tmate new-session -d "echo '.. please wait .. connecting..' && sleep 7 && tmate show-messages |tail -n1 | sed 's/\(.*ssh session:\)\(.*\)/SHARE THIS LINE (press q to quit): \2/g' |less" \; split-window -d \; attach


