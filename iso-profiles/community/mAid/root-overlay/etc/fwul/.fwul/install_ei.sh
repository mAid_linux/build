#!/bin/bash
##########################################################################
#
# Note:         This script is part of mAid (https://mAid.binbash.rocks)
# Copyright:    @@MAIDCOPYRIGHT@@
# License:      @@MAIDLICENSE@@
#
##########################################################################

source /etc/maid-release
source /etc/mAid/maid.vars

pacman -Q easy-installer || sudo $HOME/.fwul/install_package.sh pacman easy-installer
pacman -Q easy-installer && cp /usr/share/applications/e.foundation.easy-installer.desktop ~/Desktop/ && rm ~/Desktop/install-ei.desktop && chmod +x ~/Desktop/e.foundation.easy-installer.desktop
