### Tux.png

URL: https://commons.wikimedia.org/wiki/File:Tux.png

Attribution: lewing@isc.tamu.edu Larry Ewing and The GIMP (https://en.wikipedia.org/wiki/GIMP)


### mAid

The mAid logos are based on the above Tux design and have been exclusively designed by [HardStyl3r](https://gitlab.com/HardStyl3r) for mAid.

Slightly modified by [steadfasterX](https://github.com/steadfasterX) in sizes, design and adapted e.g as seen in the mAid livepatcher icon or bootloader wallpaper.

All mAid icons, logos, drawings and wallpapers are licensed under: 

**Attribution-ShareAlike 4.0 International** [(CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/)


The following is a human-readable summary of (and not a substitute for) the license [(click here for the legal code)](https://creativecommons.org/licenses/by-sa/4.0/legalcode):

You are free to:

* Share — copy and redistribute the material in any medium or format
* Adapt — remix, transform, and build upon the material

for any purpose, even commercially.

*The licensor cannot revoke these freedoms as long as you follow the license terms.*

Under the following terms:

* Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

* ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

* No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

Read the [full legal code](https://creativecommons.org/licenses/by-sa/4.0/legalcode) if you want to use or base on this work.
